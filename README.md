# Hinweise zum Projekt ZEITERFASSUNG

## Worum geht es?

Im Folgenden soll eine kleine, webbasierte App erstellt werden, auf die dann jederzeit weltweit von jedem Browser zugegriffen werden kann. Dazu wird die App zunächst auf dem lokalen Rechner erstellt und dann im nächsten Schritt auf einen Server in das Internet hochgeladen.

## Vorarbeiten:

* **Visual Studio Code** installieren und starten. Terminal in VSC durch Eingabe von **Strg+ö** öffnen.
* **Node.js** installieren. Installationserfolg überprüfen durch Eingabe von ```node --version``` im Terminal von VSC.
* **git** installieren. Installationserfolg überprüfen durch Eingabe von ```git --version``` im Terminal von VSC.
* **heroku** installieren. Installationserfolg überprüfen durch Eingabe von ```heroku --version``` im Terminal von VSC.


## Wie erstelle ich meine erste komplett neue App auf dem lokalen Server?

1. Ein neues Repository in *Bitbucket* erstellen.
2. VSC öffnen und das Repository mit ```F1``` und ```git clone``` klonen.
3. Mit ```Strg + ö``` das Terminal in VSC öffnen und ```npm init -y``` eingeben. Dadurch wird eine ```json```-Datei in den Projektordner gelegt, die Konfigurationseinstellungen enthält.
4. Installation des ```Express```-Frameworks. Der Zweck von Frameworks ist eine Vereinfachung der Programmierung. Dazu im Terminal eingeben: ```npm install --save express```
5. ... siehe Quelltext

## Wie bringe ich meine App in das Internet?

1. HEROKU muss auf dem Rechner installiert werden. Dazu auf der Seite heroku.com entsprechenden Installer herunterladen und installieren. Der Installationserfolg kann im Terminal (```Strg +  ö```) in VSC überprüft werden durch die Eingabe von ```heroku --version```
2. Im Terminal eingeben: `heroku login`
3. Im Terminal eingeben: `heroku create`
4. Im Terminal eingeben: `git add .`
5. Im Terminal eingeben: `git commit -m "zeiterfassung"`
6. Im Terminal eingeben: `git push heroku master`
7. Im Terminal eingeben: `heroku open`
8. Das Wolke- / Kreissymbol in VSC unten links klicken, um mit Bitbucket zu syncen.
Das war's. 
9. Im Browser den URL eingeben, der bei HEROKU.COM angezeigt wird.

## Wie bringe ich meine aktualisierte App in das Internet?

1. Im Terminal eingeben: `git add .`
2. Im Terminal eingeben: `git commit -m "Version2"`
3. Im Terminal eingeben: `git push heroku master`
4. Das Wolke- / Kreissymbol in VSC unten links klicken, um mit Bitbucket zu syncen.

Referenz: https://www.youtube.com/watch?v=6Fu39V6T_G0

## Wie kann ich zuhause an meinem Projekt weiterarbeiten?

1. Repository klonen
2. Im Terminal in VSC `npm install` 

# Repo for Zell's CRUD, Express and MongoDB tutorial

- [Demo](https://crud-express-mongo.herokuapp.com)
- [Tutorial](https://zellwk.com/blog/crud-express-mongodb/)

# Was kann ich tun, wenn git einen Konflikt anzeigt?

```git init```

```git remote add origin https://github.com/*.git```

```git add .```

```git commit -m "initial commit"```

```git push origin master -f```

```git push --set-upstream origin master```