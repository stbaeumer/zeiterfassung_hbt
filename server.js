var mysql = require('mysql'); 

var con = mysql.createConnection({
  host: "sql7.freesqldatabase.com",
  user: "sql7246794",
  password: "c52dK9KHJG",
  port: 3306,
  database: "sql7246794"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Datenbankverbindung hergestellt");
  
  con.query("DROP TABLE IF EXISTS taetigkeit;", function (err, result, fields) {
    if (err) throw err;
    console.log("*** Alle Tabellen: ***")
    console.log(result);
  });
  
  var sql = "CREATE TABLE IF NOT EXISTS taetigkeit (id INT AUTO_INCREMENT PRIMARY KEY, von TIMESTAMP, bis DATETIME, beschreibung varchar(80));";
  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log(result);
  });

  con.query("SHOW TABLES;", function (err, result, fields) {
    if (err) throw err;
    console.log("*** Alle Tabellen: ***")
    console.log(result);
  });

  var sql = "INSERT INTO taetigkeit (von, bis, beschreibung) VALUES (NOW(), NOW(), 'Hallo')";
  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("1 record inserted");
  });

  con.query("SELECT * FROM taetigkeit", function (err, result, fields) {
    if (err) throw err;
    console.log("*** Alle Tätigkeiten: ***")
    console.log(result);
  });
});

// Das bereits mit npm installierte Express Framework wird eingebunden:

const express = require('express')

// Das app-Objekt wird von Express erzeugt. 
// Auf das app-Objekt werden im Folgenden die Express-Methoden aufgerufen.
// Das app-Objekt repräsentiert den Server mit all seinen Einstellungen.

const app = express()

// Dem Server wird mitgeteilt, in welchem Ordner die statischen Inhalte liegen.
// Beispielsweise ist die Datei, die das Aussehen der App steuert (styles.css) dort abzulegen. 
// Statische Dateien sind Dateien, die keine Programmlogik enthalten.

app.use(express.static('public'))

// ... todo

var favicon = require('serve-favicon');
app.use(favicon(__dirname + '/public/favicon.ico'));

// Wenn man von 'Server' spricht, dann kann zum einen ein Computer gemeint sein. 
// So ein Computer hat einen eindeutigen Namen und eine eindeutige IP-Adresse, unter der er vom Client-Computer erreicht werden kann.
// Ein Server in unserem Sinne ist ein Programm, das auf einem Computer ausgeführt wird und das einen bestimmten Dienst für Clients zur Verfügung stellt.
// Weil es viele verschiedene Dienste gibt, die zeitgleich auf einem Computer ausgeführt werden können, wird jedem Dienst eine eindeutige Portnummer zugewiesen. 
// Unser Node.js-Server soll z. B. auf Port 3000 laufen. Man sagt dann: 'Der Server lauscht auf Port 3000'. 
// Weil unser Server und der Client (Browser) auf dem selben Computer laufen, kann anstelle des eindeutigen Namens bzw. der eindeutigen IP-Adresse einfach
// 'localhost' in der Browseradresszeile eingegeben werden. Zusammen mit der Portnummer schreibt man dann in die Adresszeile: 'localhost:3000'
// Zuvor muss der Server noch gestartet werden, indem im Terminal **node server.js** eingebenen wird.
// Das Terminal kann in VSC durch die Tastenkombination ***Strg+Ö*** geöffnet und geschlossen werden.
// Der Server wird angehelten mit der Tastenkombination ***Strg+C***
// Wenn alles klappt, wird der Erfolg im Terminal angezeigt:

app.listen(3000, function() {
    // Wenn alles klappt, wird der Erfolg im Terminal angezeigt:
    console.log('Der Server ist erfolgreich gestartet und lauscht auf Port 3000')
})

// Express kennt eine Methode namens get, die zwei Parameter entgegennimmt:

// 1. Der erste Parameter ist der Pfad, von dem die Anfrage kommt. '/' ist der Standardpfad.
//    Beispiel: Wenn der Anwender im Browser 'localhost:3000' eingibt, ist der Pfad '/' 
//              Wenn der Anwender im Browser 'localhost:3000/irgendwas' eingibt, ist der Pfad '/irgendwas' 
// 2. Der zweite Parameter ist die Callback-Funktion. Die sagt dem Server, wie er auf eine Anfrage vom Browser (englisch:Request) (req) mit 
//    einer Antwort (englisch: Response) (res) antworten soll.

app.get('/', function(req, res) {
    // Hier soll als Response auf den Request die index.html-Datei an den Browser zurückgegeben werden.
    res.sendFile(__dirname + '/index.html')
})

// Die app.post()-Methode leitet die Anfrage an den definierten Pfad weiter 
// mit der definierten Callback-Funktion.

app.post('/', function(req, res) {
    console.log('Hellooooooooooooooooo!')
})
